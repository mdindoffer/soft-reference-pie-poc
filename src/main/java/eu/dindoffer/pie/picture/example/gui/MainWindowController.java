package eu.dindoffer.pie.picture.example.gui;

import eu.dindoffer.pie.picture.example.core.MemoryFlooder;
import eu.dindoffer.pie.picture.example.core.PieImage;
import eu.dindoffer.pie.picture.example.core.PieLoader;
import eu.dindoffer.pie.picture.example.core.PieLoader.ImageResult;
import java.io.IOException;
import java.lang.ref.ReferenceQueue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Martin Dindoffer
 */
public class MainWindowController {

    private final PieLoader pieLoader = new PieLoader();
    private final MemoryFlooder memFlooder = new MemoryFlooder();
    private final ReferenceQueue<PieImage> imageRQ = new ReferenceQueue<>();

    @FXML
    private VBox myRoot;
    @FXML
    private Button btnShowPie;
    @FXML
    private Button btnFillMemory;
    @FXML
    private Button btnStopFilling;

    @FXML
    private void onBtnShowPieClick(ActionEvent event) throws IOException {
        ImageResult pieImg = pieLoader.loadPie(imageRQ);
        StringBuilder sb = new StringBuilder(pieImg.isCached() ? "Cached image" : "New image")
                .append(" | load time ")
                .append(Long.toString(pieImg.getLoadDuration()))
                .append("ms");
        btnFillMemory.setDisable(false);
        openImageViewer(sb.toString(), pieImg.getImg());
    }

    @FXML
    private void onBtnFillMemory(ActionEvent event) {
        btnFillMemory.setDisable(true);
        btnShowPie.setDisable(true);
        btnStopFilling.setDisable(false);
        new Thread(new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                memFlooder.floodHeap(imageRQ);
                return null;
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                btnShowPie.setDisable(false);
                btnStopFilling.setDisable(true);
            }
        }).start();
    }

    private void openImageViewer(String title, Image img) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ImageViewer.fxml"));
        Parent root = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.setMaximized(false);
        stage.setTitle(title);
        ImageViewerController ivController = fxmlLoader.<ImageViewerController>getController();
        ivController.setImage(img);
        stage.setScene(new Scene(root));
        stage.show();
    }

    @FXML
    private void onBtnStopFilling(ActionEvent event) {
        memFlooder.stop();
    }
}
