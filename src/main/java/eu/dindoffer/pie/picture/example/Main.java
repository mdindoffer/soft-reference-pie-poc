package eu.dindoffer.pie.picture.example;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * -Xms96m
 * -Xmx96m
 * -XX:-UseGCOverheadLimit
 * 
 * @author Martin Dindoffer
 */
public class Main extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        Parent root  = FXMLLoader.load(getClass().getClassLoader().getResource("MainWindow.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Soft reference pie example");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
