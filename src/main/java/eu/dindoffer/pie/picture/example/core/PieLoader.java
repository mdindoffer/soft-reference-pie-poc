package eu.dindoffer.pie.picture.example.core;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;

/**
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public class PieLoader {

    private SoftReference<PieImage> pie = new SoftReference<>(null);

    public ImageResult loadPie(ReferenceQueue<PieImage> imageRQ) {
        long startTS = System.currentTimeMillis();
        PieImage img = pie.get();
        boolean cached;
        if (img == null) {
            cached = false;
            System.out.println("Image already destroyed, loading again");
            img = new PieImage(this.getClass().getClassLoader().getResourceAsStream("apple_pie.bmp"));
            this.pie = new SoftReference<>(img, imageRQ);
        } else {
            cached = true;
            System.out.println("Returning cached image");
        }
        return new ImageResult(img, System.currentTimeMillis() - startTS, cached);
    }

    public static class ImageResult {

        private final PieImage img;
        private final long loadDuration;
        private final boolean cached;

        public ImageResult(PieImage img, long loadDuration, boolean cached) {
            this.img = img;
            this.loadDuration = loadDuration;
            this.cached = cached;
        }

        public PieImage getImg() {
            return img;
        }

        public long getLoadDuration() {
            return loadDuration;
        }

        public boolean isCached() {
            return cached;
        }
    }
}
