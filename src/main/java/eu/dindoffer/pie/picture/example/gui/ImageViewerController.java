package eu.dindoffer.pie.picture.example.gui;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author Martin Dindoffer
 */
public class ImageViewerController {

    @FXML
    private ImageView imvImage;

    public void setImage(Image img) {
        imvImage.setImage(img);
    }

}
