package eu.dindoffer.pie.picture.example.core;

import java.lang.ref.ReferenceQueue;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public class MemoryFlooder {

    private final List<byte[]> memHogs = new LinkedList<>();
    private volatile boolean fillMemory;

    public void floodHeap(ReferenceQueue refQ) {
        fillMemory = true;
        while (fillMemory) {
            printAvailableMem();
            if (refQ.poll() != null) {
                System.out.println("Image has been removed from memory");
                this.memHogs.clear();
                break;
            }
            byte[] memHog = new byte[1];
            memHogs.add(memHog);
        }
    }

    public void stop() {
        this.fillMemory = false;
    }

    private long printAvailableMem() {
        Runtime runtime = Runtime.getRuntime();
        long totalMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();
        long maxMemory = runtime.maxMemory();
        long usedMemory = totalMemory - freeMemory;
        long availableMemory = maxMemory - usedMemory;
        System.out.println("avail mem: " + availableMemory);
        return availableMemory;
    }
}
