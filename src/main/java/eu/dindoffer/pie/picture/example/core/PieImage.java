package eu.dindoffer.pie.picture.example.core;

import java.io.InputStream;
import javafx.scene.image.Image;

/**
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public class PieImage extends Image {

    public PieImage(InputStream is) {
        super(is);
    }

}
